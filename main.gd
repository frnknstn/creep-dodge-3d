extends Node


export (PackedScene) var mob_scene


func _process(delta):
	OS.set_window_title("Creep Dodge 3D"  + " - " + str(Engine.get_frames_per_second()) + " fps")


func _ready():
	randomize()
	$UserInterface/Retry.hide()


func _on_MobTimer_timeout():
	var mob = mob_scene.instance()
	
	# Choose a random location on Path2D.
	$SpawnPath/SpawnLocation.unit_offset = randf()
	
	add_child(mob)
	print($SpawnPath/SpawnLocation.translation)
	mob.initialize($SpawnPath/SpawnLocation.translation, $Player.transform.origin)
	mob.connect("squashed", $UserInterface/ScoreLabel, "_on_Mob_squashed")


func _on_Player_hit():
	$MobTimer.stop()
	$UserInterface/Retry.show()


func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and $UserInterface/Retry.visible:
		# This restarts the current scene.
		get_tree().reload_current_scene()
